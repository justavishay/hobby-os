#include "frame_buffer.h"
#include "serial_port.h"
#include "gdt.h"
#include "idt.h"
#include "keyboard.h"
#include "user_interface.h"
#include "isr.h"
#include "ide.h"
#include "heap.h"
#include "timer.h"
#include "string.h"
#include "ext2.h"

/* The I/O ports */
#define FB_COMMAND_PORT 0x3D4
#define FB_DATA_PORT 0x3D5

/* The I/O port commands */
#define FB_HIGH_BYTE_COMMAND 14
#define FB_LOW_BYTE_COMMAND 15

#define FB_GREEN 2
#define FB_BLACK 0

#define BUFFER_LEN 128

void print_menu()
{
  printf("Welcome to our file system!\n");
  printf("Here are your possible commands:\n");
  printf("touch <file_path> - creates a file\n");
  printf("mkdir <folder_path> - creates a folder\n");
  printf("write <file_path> <data> - writes to a file\n");
  printf("print <file_path> - print the file\n");
  printf("rm <path> - deletes a file or a folder\n");
  printf("cp <source_file_path> <destination_file_path> - copies the file\n");
  printf("cd <folder_path> - change directory\n");
  printf("ls - print current directory\n");
  printf("help - print this menu\n");
}

char *remove_quotes(char *str)
{
  if (str[0] == '\'')
  {
    str++;
  }

  uint32 i = 0;
  while (str[i])
  {
    if (str[i] == '\'')
    {
      str[i] = 0;
    }
    i++;
  }

  return str;
}

char *get_next_part(char *str)
{
  char *next_part = str + strlen(str) + 1;
  next_part = remove_quotes(next_part);
  return next_part;
}

void process_command(char *command)
{
  strsplit(command, ' ');
  char *arg1 = get_next_part(command);

  if (strcmp(command, "ls") && strcmp(command, "help") && !strlen(arg1))
  {
    printf("First argument cannot be empty\n");
    return;
  }

  if (!strcmp(command, "touch"))
  {
    char *file_name = strrchr(arg1, '/');
    sint32 error_code = 0;
    if (file_name == NULL) // meaning the path was only the file name so the actual part of the name is arg1
    {
      error_code = create_file("", arg1); // "" indicates the active dir
    }
    else
    {
      // to discard the '/'
      file_name[0] = 0;
      file_name++;
      if (!strlen(arg1)) // meaning it was originally '/file_name' - from the root
      {
        error_code = create_file("/", file_name);
      }
      else
      {
        error_code = create_file(arg1, file_name);
      }
    }

    if (error_code == DIRECTORY_NOT_CREATED)
    {
      printf("Path to file doesn't exist\n");
    }
  }
  else if (!strcmp(command, "mkdir"))
  {
    char *folder_name = strrchr(arg1, '/');
    sint32 error_code = 0;
    if (folder_name == NULL) // meaning the path was only the folder name so the actual part of the name is arg1
    {
      error_code = create_directory("", arg1); // "" indicates the active dir
    }
    else
    {
      // to discard the '/'
      folder_name[0] = 0;
      folder_name++;
      if (!strlen(arg1)) // meaning it was originally '/file_name' - relative to the root
      {
        error_code = create_directory("/", folder_name);
      }
      else
      {
        error_code = create_directory(arg1, folder_name);
      }
    }

    if (error_code == DIRECTORY_NOT_CREATED)
    {
      printf("Path to folder doesn't exist\n");
    }
  }
  else if (!strcmp(command, "write"))
  {
    char *arg2 = get_next_part(arg1);
    if (!strlen(arg2))
    {
      printf("Second argument cannot be empty\n");
      return;
    }

    sint32 error_code = write_to_file(arg1, arg2, strlen(arg2));
    if (error_code == INCORRECT_TYPE)
    {
      printf("You can't write to a folder\n");
    }
    else if (error_code == INODE_NOT_FOUND)
    {
      printf("File doesn't exist\n");
    }
  }
  else if (!strcmp(command, "print"))
  {
    sint32 data = get_file_content(arg1);

    if (data == INCORRECT_TYPE)
    {
      printf("You can't print a folder\n");
    }
    else if(data == INODE_NOT_FOUND)
    {
      printf("File doesn't exist\n");
    }
    else if(data == ORIGINAL_DESTROYED)
    {
      printf("The original file was deleted\n");
    }
    else if(data == NULL)
    {
      printf("This file doesn't have any data\n");
    }
    else
    {
      printf("%s\n", data);
      free(data);
    }
  }
  else if (!strcmp(command, "rm"))
  {
    char *inode_name = strrchr(arg1, '/');
    sint32 error_code = 0;
    if (inode_name == NULL) // meaning the path was only the folder name so the actual part of the name is arg1
    {
      error_code = delete_from_folder("", arg1); // "" indicates the active dir
    }
    else
    {
      // to discard the '/'
      inode_name[0] = 0;
      inode_name++;
      if (!strlen(arg1)) // meaning it was originally '/file_name' - relative to the root
      {
        error_code = delete_from_folder("/", inode_name);
      }
      else
      {
        error_code = delete_from_folder(arg1, inode_name);
      }
    }

    if (error_code == DIRECTORY_NOT_CREATED)
    {
      printf("Path to file or folder doesn't exist\n");
    }
  }
  else if (!strcmp(command, "cp"))
  {
    char *arg2 = get_next_part(arg1);
    if (!strlen(arg2))
    {
      printf("Second argument cannot be empty\n");
      return;
    }

    sint32 error_code = 0;
    char* destination_inode_name = strrchr(arg2, '/');
    if(destination_inode_name == NULL) // meaning the path was only the new file name so the actual part of the name is arg2
    {
      error_code = copy_file(arg1, "", arg2); // "" indicates the active dir
    }
    else
    {
      // to discard the '/'
      destination_inode_name[0] = 0;
      destination_inode_name++;
      if (!strlen(arg2)) // meaning it was originally '/file_name' - relative to the root
      {
        error_code = copy_file(arg1, "/", destination_inode_name);
      }
      else
      {
        error_code = copy_file(arg1, arg2, destination_inode_name);
      }
    }

    if (error_code == INCORRECT_TYPE)
    {
      printf("You can't copy a folder\n");
    }
    else if (error_code == INODE_NOT_FOUND)
    {
      printf("File doesn't exist\n");
    }
  }
  else if (!strcmp(command, "cd"))
  {
    sint32 error_code = change_directory(arg1);
    if(error_code == INCORRECT_TYPE)
    {
      printf("You can't change directory to a file\n");
    }
  }
  else if (!strcmp(command, "ls"))
  {
    print_current_directory();
  }
  else if (!strcmp(command, "help"))
  {
    print_menu();
  }
  else
  {
    printf("Invalid command, please try again\n");
  }
}

int main()
{
  init_gdt();
  init_idt();
  serial_configure(SERIAL_COM1_BASE, Baud_115200);
  serial_write(SERIAL_COM1_BASE, "something", 9);
  clear_screen();
  ata_init();
  init_timer(50);
  init((void*)0x001012000, (void*)0x001016000);
  clear_screen();

  // ext2_firstInit();
  ext2_init();

  //asm volatile ("int $33");

  return 0;
}