#ifndef INCLUDE_EXT2_H
#define INCLUDE_EXT2_H

#pragma once
#include "types.h"
#include "heap.h"
#include "string.h"
#include "ide.h"
#include "user_interface.h"
#include "serial_port.h"

#define AMOUNT_OF_DATA_BLOCKS 12
#define MAX_NAME_SIZE 255;

#define EXT2_FT_DIR 2
#define EXT2_FT_FILE 1
#define EXT2_ROOT_INODE 0

#define INODE_NOT_FOUND -1
#define NOT_FULL_PATH -2
#define DIRECTORY_NOT_CREATED -1
#define INCORRECT_TYPE -3
#define ORIGINAL_DESTROYED -4
#define OPERATION_SUCCESSFULL 0

#define FOLDER_MODE 0xdb20
#define FILE_MODE 0xdb40

typedef struct inode
{
  uint16 mode;
  uint16 user_id;
  uint32 size;
  uint32 atime;
  uint32 ctime;
  uint32 mtime;
  uint32 dtime;
  uint16 gid;
  uint16 hard_links;
  uint32 flags;
  uint32 direct_block_pointers[AMOUNT_OF_DATA_BLOCKS];
  uint32 single_indirect_block_pointer;
  uint32 double_indirect_block_pointer;
  uint32 triple_indirect_block_pointer;
} __attribute__((packed)) inode_t;

typedef struct superblock
{
  uint32 inodesCount;
  uint32 blocksCount;
  uint32 rBlocksCount;
  uint32 freeBlocksCount;
  uint32 freeInodesCount;
  uint32 firstDataBlock;
  uint8 blockSize;
  uint8 fragmentSize;
  uint32 blocksPerGroup;
  uint32 fragsPerGroup;
  uint32 inodesPerGroup;
  uint16 magic;
  uint16 state;
  uint16 blockGroupNum;
} __attribute__((packed)) superblock_t;

typedef struct
{
  uint32 blockBitmap;
  uint32 inodeBitmap;
  uint32 inodeTable;
  uint16 freeBlocksCount;
  uint16 freeInodesCount;
  uint16 usedDirsCount;
  uint16 pad[7];
} __attribute__((packed)) group_descriptor_t;

typedef struct dirent
{
  uint32 inode;
  uint16 dirent_size;
  uint16 name_len;
  uint8 type;
  uint8 name[32];
} __attribute__((packed)) dirent_t;

superblock_t *readSuperblock();
group_descriptor_t *parseGroupDescriptor(uint32 block_group);

uint32 find_free_bit(unsigned char *bitmap, uint32 size);
uint8 get_bit(uint32 address, unsigned char *bitmap);
void mark_bit(uint32 address, unsigned char *bitmap, uint8 state);

uint32 create_inode(inode_t *inode_data);
void free_inode(uint32 inode_number);
uint8 delete_from_folder(const char *parent_path, const char *name);
uint32 create_folder_inode();
uint32 create_file_inode();
inode_t *get_inode(uint32 inode_number);
void write_inode(uint32 inode_number, const inode_t *inode);
uint8 write_inode_data(uint32 inode_number, uint32 size, uint32 data);
uint8 append_inode_data(uint32 inode_number, uint32 new_data_size, uint32 data);
sint32 get_inode_number_by_path(const char *path);
uint32 getInodeData(inode_t inode);

sint32 create_directory(const char *parent_path, const char *folder_name);
uint32 search_directory_entry(const inode_t *directory_inode, const char *name);
sint32 change_directory(const char *dir_path);
void delete_directory_recursive(uint32 parent_inode_number);
void printDirectories(uint32 inode_number);
void print_current_directory();

sint32 create_file(const char *parent_path, const char *file_name);
sint32 write_to_file(const char* file_path, uint32 data, uint32 size);
sint32 get_file_content(const char* file_path);
sint32 copy_file(const char* source_file_path, const char* destination_parent_path, const char* destination_file_name);

uint32 allocate_block();
void deallocate_block(uint32 address);
uint32 write_to_pointers(uint32 data, uint32 size, uint32 *pointers, uint32 pointers_count);
uint32 complete_block(uint32 block_num, uint32 data_in_last_block, uint32 data_size, uint32 data);

void ext2_init();
void ext2_firstInit();

#endif /* INCLUDE_EXT2_H */