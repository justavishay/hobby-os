#include "string.h"

void reverse_string(char str[], int length)
{
  int start = 0;
  int end = length - 1;
  while (start < end)
  {
    char temp = str[start];
    str[start] = str[end];
    str[end] = temp;
    end--;
    start++;
  }
}

char *int_to_string(int num, char *str, int base)
{
  int i = 0;
  int is_negative = 0;

  /* Handle 0 explicitly, otherwise empty string is
   * printed for 0 */
  if (num == 0)
  {
    str[i] = '0';
    str[i + 1] = '\0';
    return str;
  }

  // In standard itoa(), negative numbers are handled
  // only with base 10. Otherwise numbers are
  // considered unsigned.
  if (num < 0 && base == 10)
  {
    is_negative = 1;
    num = -num;
  }

  // Process individual digits
  while (num != 0)
  {
    int rem = num % base;
    str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
    num = num / base;
  }

  // If number is negative, append '-'
  if (is_negative)
    str[i++] = '-';

  str[i] = '\0'; // Append string terminator

  // Reverse the string
  reverse_string(str, i);

  return str;
}

uint32 strsplit(char *str, char delim)
{
  uint32 n = 0;
  uint32 i = 0;
  uint8 shouldSplit = 1; // makes it so when we split the command from the user, if he types in parameters with "" then we will consider everything in it as a single parameter
  while (str[i])
  {
    if (shouldSplit && str[i] == delim)
    {
      str[i] = 0;
      n++;
    }
    else if (str[i] == '\'')
    {
      shouldSplit = !shouldSplit;
    }
    i++;
  }
  n++;
  return n;
}

void *memset(void *dst, char c, uint32 n)
{
  char *temp = dst;
  for (; n != 0; n--)
    *temp++ = c;
  return dst;
}

void *memcpy(void *dst, const void *src, uint32 n)
{
  char *ret = dst;
  char *p = dst;
  const char *q = src;
  while (n--)
    *p++ = *q++;
  return ret;
}

void *memmove(void *dest, const void *src, uint32 n)
{
  unsigned char isCopyRequire = 0; // flag bit
  char *pcSource = (char *)src;
  char *pcDstn = (char *)dest;
  // return if pcDstn and pcSource is NULL
  if ((pcSource == 0) || (pcDstn == 0))
  {
    return 0;
  }
  // overlap buffer
  if ((pcSource < pcDstn) && (pcDstn < pcSource + n))
  {
    for (pcDstn += n, pcSource += n; n--;)
    {
      *--pcDstn = *--pcSource;
    }
  }
  else
  {
    while (n--)
    {
      *pcDstn++ = *pcSource++;
    }
  }
  return dest;
}

uint8 strncmp(char *a, char *b, uint32 n)
{
  uint32 i;
  for (i = 0; i < n; ++i)
  {
    if (a[i] != b[i])
    {
      return 1;
    }
  }
  return 0;
}

uint8 memcmp(uint8 *s1, uint8 *s2, uint32 n)
{
  while (n--)
  {
    if (*s1 != *s2)
      return 0;
    s1++;
    s2++;
  }
  return 1;
}

uint32 strlen(const char *s)
{
  int len = 0;
  while (*s++)
    len++;
  return len;
}

uint8 strcmp(const char *s1, char *s2)
{
  int i = 0;

  while ((s1[i] == s2[i]))
  {
    if (s2[i++] == 0)
      return 0;
  }
  return 1;
}

uint8 strcpy(char *dst, const char *src)
{
  int i = 0;
  while ((*dst++ = *src++) != 0)
    i++;
  return i;
}

void strcat(char *dest, const char *src)
{
  char *end = (char *)dest + strlen(dest);
  memcpy((void *)end, (void *)src, strlen(src));
  end = end + strlen(src);
  *end = '\0';
}

void strncat(char *dest, const char *src, int n)
{
  char *end = (char *)dest + strlen(dest);
  memcpy((void *)end, (void *)src, n);
  end = end + n;
  *end = '\0';
}

uint8 isspace(char c)
{
  return c == ' ' || c == '\t' || c == '\n' || c == '\v' || c == '\f' || c == '\r';
}

uint8 isalpha(char c)
{
  return (((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z')));
}

char upper(char c)
{
  if ((c >= 'a') && (c <= 'z'))
    return (c - 32);
  return c;
}

char lower(char c)
{
  if ((c >= 'A') && (c <= 'Z'))
    return (c + 32);
  return c;
}

void itoa(char *buf, int base, int d)
{
  char *p = buf;
  char *p1, *p2;
  unsigned long ud = d;
  int divisor = 10;

  /* If %d is specified and D is minus, put ‘-’ in the head. */
  if (base == 'd' && d < 0)
  {
    *p++ = '-';
    buf++;
    ud = -d;
  }
  else if (base == 'x')
    divisor = 16;

  /* Divide UD by DIVISOR until UD == 0. */
  do
  {
    int remainder = ud % divisor;
    *p++ = (remainder < 10) ? remainder + '0' : remainder + 'a' - 10;
  } while (ud /= divisor);

  /* Terminate BUF. */
  *p = 0;

  /* Reverse BUF. */
  p1 = buf;
  p2 = p - 1;
  while (p1 < p2)
  {
    char tmp = *p1;
    *p1 = *p2;
    *p2 = tmp;
    p1++;
    p2--;
  }
}

#define NULL (void *)0

char *strchr(const char *str, int character)
{
  while (*str != '\0')
  {
    if (*str == character)
    {
      return (char *)str;
    }
    ++str;
  }

  // Return NULL if the character is not found
  return NULL;
}

char *strrchr(const char* str, int character)
{
  sint32 len = strlen(str);
  while(--len >= 0)
  {
    if(str[len] == character)
    {
      return (char*)(str + len);
    }
  }

  return NULL;
}

char *strtok(char *str, const char *delim)
{
  static char *saved_ptr = NULL;

  // If called with NULL, continue from the last saved position
  if (str == NULL)
  {
    str = saved_ptr;
  }

  // If the input string is NULL or empty, return NULL
  if (str == NULL || *str == '\0')
  {
    saved_ptr = NULL;
    return NULL;
  }

  // Skip leading delimiters
  while (*str != '\0' && strchr(delim, *str) != NULL)
  {
    ++str;
  }

  // If we reached the end of the string, return NULL
  if (*str == '\0')
  {
    saved_ptr = NULL;
    return NULL;
  }

  // Find the end of the token
  char *token_end = str;
  while (*token_end != '\0' && strchr(delim, *token_end) == NULL)
  {
    ++token_end;
  }

  // If the token is not empty, save the position for the next call
  if (str != token_end)
  {
    saved_ptr = token_end;
    if (*token_end != '\0')
    {
      *token_end = '\0'; // Null-terminate the token
      ++saved_ptr;       // Move saved_ptr to the next character
    }
    return str;
  }

  saved_ptr = NULL;
  return NULL;
}