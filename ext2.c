#include "ext2.h"

#define DRIVE 0 // We are using drive 0, the harddisk
#define BYTE_SIZE 8
#define BLOCK_SIZE 1024
#define ERROR_CODE 1
#define BLOCKS_COUNT 8192
#define INODES_COUNT 256

superblock_t *sb;
group_descriptor_t *gd;
unsigned char block_bitmap[BLOCKS_COUNT / BYTE_SIZE] = {0};
unsigned char inode_bitmap[INODES_COUNT / BYTE_SIZE] = {0};
uint32 active_dir_inode_number = EXT2_ROOT_INODE;

superblock_t *read_superblock()
{
  superblock_t *superBlock = malloc(sizeof(superblock_t));
  ide_read(0, sizeof(superblock_t), superBlock);
  return superBlock;
}

group_descriptor_t *read_group_descriptor()
{
  group_descriptor_t *group_descriptor = malloc(sizeof(group_descriptor_t));
  ide_read(BLOCK_SIZE, sizeof(group_descriptor_t), group_descriptor);
  return group_descriptor;
}

uint32 find_free_bit(unsigned char *bitmap, uint32 size)
{
  for (uint32 byte_index = 0; byte_index < size; byte_index++)
  {
    for (uint32 bit_index = 0; bit_index < BYTE_SIZE; bit_index++)
    {
      if (!((bitmap)[byte_index] & (1 << bit_index)))
      {
        return byte_index * 8 + bit_index;
      }
    }
  }
}

void mark_bit(uint32 address, unsigned char *bitmap, uint8 state)
{
  uint32 byte_pos = address / BYTE_SIZE;
  uint8 bit_pos = address % BYTE_SIZE;
  if (state)
  {
    bitmap[byte_pos] |= (1 << bit_pos);
  }
  else
  {
    bitmap[byte_pos] &= ~(1 << bit_pos);
  }
}

uint8 get_bit(uint32 address, unsigned char *bitmap)
{
  return (bitmap[address / 8] >> (address % 8)) & 1;
}

void print_bitmap(unsigned char *bitmap, uint32 size)
{
  printf("\n");
  uint32 ones = 0;
  uint32 zeros = 0;
  for (uint32 i = 0; i < size; i++)
  {
    for (uint32 j = 0; j < BYTE_SIZE; j++)
    {
      uint8 value = (bitmap[i] >> j) & 1;
      if (value)
      {
        ones++;
      }
      else
      {
        zeros++;
      }
      printf("%d", (bitmap[i] >> j) & 1);
    }
  }

  printf("ones: %d, zeros: %d\n\n", ones, zeros);
}

uint32 create_inode(inode_t *inode_data)
{
  uint32 free_bit_addr = find_free_bit(inode_bitmap, INODES_COUNT / BYTE_SIZE);
  mark_bit(free_bit_addr, inode_bitmap, 1);
  ide_write(gd->inodeBitmap, INODES_COUNT / BYTE_SIZE, inode_bitmap);
  for (int i = 0; i < AMOUNT_OF_DATA_BLOCKS; i++)
  {
    inode_data->direct_block_pointers[i] = NULL;
  }
  inode_data->single_indirect_block_pointer = NULL;
  inode_data->double_indirect_block_pointer = NULL;
  ide_write(gd->inodeTable + free_bit_addr * sizeof(inode_t), sizeof(inode_t), inode_data);
  sb->freeInodesCount--;
  gd->freeInodesCount--;
  ide_write(0, sizeof(superblock_t), sb);
  ide_write(BLOCK_SIZE, sizeof(group_descriptor_t), gd);
  return free_bit_addr;
}

uint32 create_folder_inode()
{
  inode_t *folder_inode = malloc(sizeof(inode_t));
  folder_inode->mode = FOLDER_MODE; // a folder that everyone can read/write
  folder_inode->user_id = 0;
  folder_inode->size = 0;
  // We need some way of managing "time"
  folder_inode->atime = 0;
  folder_inode->ctime = 0;
  folder_inode->mtime = 0;
  folder_inode->dtime = 0;
  folder_inode->gid = 0;
  folder_inode->hard_links = 1;
  folder_inode->flags = 0;

  uint32 inode_number = create_inode(folder_inode);

  free(folder_inode);
  return inode_number;
}

uint32 create_file_inode()
{
  inode_t *file_inode = malloc(sizeof(inode_t));
  file_inode->mode = FILE_MODE; // a file that everyone can read/write
  file_inode->user_id = 0;
  file_inode->size = 0;
  // We need some way of managing "time"
  file_inode->atime = 0;
  file_inode->ctime = 0;
  file_inode->mtime = 0;
  file_inode->dtime = 0;
  file_inode->gid = 0;
  file_inode->hard_links = 1;
  file_inode->flags = 0;

  uint32 inode_number = create_inode(file_inode);

  free(file_inode);
  return inode_number;
}

void ext2_init()
{
  sb = read_superblock();
  gd = read_group_descriptor();
  ide_read(gd->blockBitmap, BLOCKS_COUNT / BYTE_SIZE, block_bitmap);
  ide_read(gd->inodeBitmap, INODES_COUNT / BYTE_SIZE, inode_bitmap);
  print_bitmap(block_bitmap, 8);
  print_bitmap(inode_bitmap, INODES_COUNT / BYTE_SIZE);
  if (sb->magic != 0xEF53)
  {
    printf("Error! Filesystem is NOT EXT2!\n");
  }
  else
  {
    printf("Filesystem is EXT2!\n");
  }
}

void ext2_firstInit()
{
  superblock_t *superBlock = malloc(sizeof(superblock_t));
  superBlock->inodesCount = INODES_COUNT;
  superBlock->blocksCount = BLOCKS_COUNT;
  superBlock->rBlocksCount = 72;
  superBlock->freeBlocksCount = BLOCKS_COUNT;
  superBlock->freeInodesCount = INODES_COUNT;
  superBlock->firstDataBlock = 0;
  superBlock->blockSize = 0;
  superBlock->fragmentSize = 0;
  superBlock->blocksPerGroup = BLOCKS_COUNT;
  superBlock->fragsPerGroup = 0;
  superBlock->inodesPerGroup = INODES_COUNT;
  superBlock->magic = 0xEF53;
  superBlock->state = 0;
  superBlock->blockGroupNum = 1;
  ide_write(0, sizeof(superblock_t), superBlock);

  group_descriptor_t *groupDescriptor = malloc(sizeof(group_descriptor_t));
  groupDescriptor->blockBitmap = 2 * BLOCK_SIZE;
  groupDescriptor->inodeBitmap = groupDescriptor->blockBitmap + superBlock->blocksCount / BYTE_SIZE + BLOCK_SIZE;
  groupDescriptor->inodeTable = groupDescriptor->inodeBitmap + BLOCK_SIZE;
  groupDescriptor->freeBlocksCount = BLOCKS_COUNT;
  groupDescriptor->freeInodesCount = INODES_COUNT;
  groupDescriptor->usedDirsCount = 0;
  groupDescriptor->pad[7];
  ide_write(BLOCK_SIZE, sizeof(group_descriptor_t), groupDescriptor);
  ide_write(groupDescriptor->inodeBitmap, INODES_COUNT / BYTE_SIZE, inode_bitmap);

  uint32 initial_block_bitmap = (1 << (groupDescriptor->inodeTable + sizeof(inode_t) * superBlock->inodesCount) / BLOCK_SIZE + 1) - 1; // the first 4 blocks are taken by the superblock, block_group_descriptor and the bitmaps. the taken blocks that are calculated here are the inode table blocks and the length of the blocks bitmap
  ide_write(groupDescriptor->blockBitmap, sizeof(uint32), &initial_block_bitmap);

  unsigned char inode_bitmap_init[INODES_COUNT / BYTE_SIZE] = {0};
  ide_write(groupDescriptor->inodeBitmap, INODES_COUNT / BYTE_SIZE, inode_bitmap_init);

  free(groupDescriptor);
  free(superBlock);

  ext2_init();
  create_directory("", "/");
}

uint32 getInodeData(inode_t inode)
{
  uint32 size = inode.size;
  uint32 data = malloc(size + size % BLOCK_SIZE);
  if (data == NULL)
  {
    return NULL;
  }
  uint32 currentDataPtr = data;
  for (int i = 0; size > 0 && i < AMOUNT_OF_DATA_BLOCKS; ++i)
  {
    if (inode.direct_block_pointers[i] != 0)
    {
      ide_read(inode.direct_block_pointers[i] * BLOCK_SIZE, BLOCK_SIZE, currentDataPtr);
      currentDataPtr += BLOCK_SIZE;
      size -= BLOCK_SIZE;
    }
  }
  if (inode.single_indirect_block_pointer != 0)
  {
    uint32 *blockData = malloc(BLOCK_SIZE);
    ide_read(inode.single_indirect_block_pointer * BLOCK_SIZE, BLOCK_SIZE, blockData);
    for (int i = 0; size > 0 && i < BLOCK_SIZE / sizeof(uint32); ++i)
    {
      if (blockData[i] != NULL)
      {
        ide_read(blockData[i] * BLOCK_SIZE, BLOCK_SIZE, currentDataPtr);
        currentDataPtr += BLOCK_SIZE;
        size -= BLOCK_SIZE;
      }
    }
    free(blockData);
  }
  // if we need to expand the data we will implement double & triple indirect (now we have 256MB for each inode) *too much
  return data;
}

inode_t *get_inode(uint32 inode_number)
{
  uint8 inode_bit = get_bit(inode_number, inode_bitmap);

  if (!inode_bit)
  {
    return NULL;
  }

  inode_t *inode = malloc(sizeof(inode_t));
  ide_read(gd->inodeTable + inode_number * sizeof(inode_t), sizeof(inode_t), inode);
  return inode;
}

uint32 allocate_block()
{
  uint32 free_block = find_free_bit(block_bitmap, BLOCKS_COUNT);
  mark_bit(free_block, block_bitmap, 1);
  ide_write(gd->blockBitmap, BLOCKS_COUNT / BYTE_SIZE, block_bitmap);
  sb->freeBlocksCount--;
  gd->freeBlocksCount--;
  ide_write(0, sizeof(superblock_t), sb);
  ide_write(BLOCK_SIZE, sizeof(group_descriptor_t), gd);
  return free_block;
}

void deallocate_block(uint32 address)
{
  mark_bit(address, block_bitmap, 0);
  ide_write(gd->blockBitmap, BLOCKS_COUNT / BYTE_SIZE, block_bitmap);
  sb->freeBlocksCount++;
  gd->freeBlocksCount++;
  ide_write(0, sizeof(superblock_t), sb);
  ide_write(BLOCK_SIZE, sizeof(group_descriptor_t), gd);
}

uint32 write_to_pointers(uint32 data, uint32 size, uint32 *pointers, uint32 pointers_count)
{
  uint32 i = 0;

  for (; size > 0 && i < pointers_count; i++)
  {
    if (pointers[i] == 0)
    {
      pointers[i] = allocate_block();
    }

    uint16 size_to_write = size > BLOCK_SIZE ? BLOCK_SIZE : size;
    ide_write(pointers[i] * BLOCK_SIZE, size_to_write, data);
    data += BLOCK_SIZE;
    size -= size_to_write;
  }

  for (; i < size; i++)
  {
    if (pointers[i] == 0)
    {
      break;
    }

    deallocate_block(pointers[i]);
    pointers[i] = 0;
  }

  return size;
}

uint8 write_inode_data(uint32 inode_number, uint32 size, uint32 data)
{
  inode_t *inode = get_inode(inode_number);

  if (inode == NULL)
  {
    return ERROR_CODE;
  }

  inode->size = size;
  size = write_to_pointers(data, size, inode->direct_block_pointers, AMOUNT_OF_DATA_BLOCKS);
  data += inode->size - size;

  if (size > 0)
  {
    if (inode->single_indirect_block_pointer == 0)
    {
      inode->single_indirect_block_pointer = allocate_block();
    }

    uint32 *block_data = malloc(BLOCK_SIZE);

    ide_read(inode->single_indirect_block_pointer * BLOCK_SIZE, BLOCK_SIZE, block_data);
    write_to_pointers(data, size, block_data, BLOCK_SIZE / sizeof(uint32));
    ide_write(inode->single_indirect_block_pointer * BLOCK_SIZE, BLOCK_SIZE, block_data);

    free(block_data);
  }
  else if (inode->single_indirect_block_pointer != 0)
  {
    deallocate_block(inode->single_indirect_block_pointer);
    inode->single_indirect_block_pointer = 0;
  }

  ide_write(gd->inodeTable + sizeof(inode_t) * inode_number, sizeof(inode_t), inode);
  free(inode);
  return 0;
}

void write_inode(uint32 inode_number, const inode_t *inode)
{
  uint8 inode_bit = get_bit(inode_number, inode_bitmap);

  if (!inode_bit)
    return;
  ide_write(gd->inodeTable + inode_number * sizeof(inode_t), sizeof(inode_t), inode);
}

uint32 complete_block(uint32 block_num, uint32 data_in_last_block, uint32 data_size, uint32 data)
{
  uint32 remaining_space = BLOCK_SIZE - data_in_last_block;
  uint32 size_to_write = data_size > remaining_space ? remaining_space : data_size;
  ide_write(block_num * BLOCK_SIZE + data_in_last_block, size_to_write, data);
  return size_to_write;
}

uint8 append_inode_data(uint32 inode_number, uint32 new_data_size, uint32 data)
{
  inode_t *inode = get_inode(inode_number);

  if (inode == NULL)
  {
    return ERROR_CODE;
  }

  const original_new_data_size = new_data_size;
  uint32 current_ptr_num = inode->size / BLOCK_SIZE;
  uint32 data_in_last_block = inode->size % BLOCK_SIZE;
  inode->size = inode->size + new_data_size;

  if (current_ptr_num < AMOUNT_OF_DATA_BLOCKS)
  {
    if (data_in_last_block)
    {
      uint32 size_written = complete_block(inode->direct_block_pointers[current_ptr_num], data_in_last_block, new_data_size, data);
      data += size_written;
      new_data_size -= size_written;
      current_ptr_num++;
    }

    data_in_last_block = 0; // in case it wasn't and there's still more data after the direct pointers

    // If it's still the case after writing to the incomplete block and there's more data to write, then we can continue writing to the direct block pointers
    if (current_ptr_num < AMOUNT_OF_DATA_BLOCKS && new_data_size > 0)
    {
      new_data_size = write_to_pointers(data, new_data_size, inode->direct_block_pointers + current_ptr_num, AMOUNT_OF_DATA_BLOCKS - current_ptr_num);
      data += original_new_data_size - new_data_size;
    }
  }
  else
  {
    current_ptr_num -= AMOUNT_OF_DATA_BLOCKS;
  }

  if (new_data_size > 0)
  {
    if (inode->single_indirect_block_pointer == NULL)
    {
      inode->single_indirect_block_pointer = allocate_block();
    }

    uint32 *block_data = malloc(BLOCK_SIZE);
    ide_read(inode->single_indirect_block_pointer * BLOCK_SIZE, BLOCK_SIZE, block_data);

    if (data_in_last_block)
    {
      uint32 size_written = complete_block(block_data[current_ptr_num], data_in_last_block, new_data_size, data);
      data += size_written;
      new_data_size -= size_written;
      current_ptr_num++;
    }

    // If there's still more data to write after completing the block
    if (new_data_size > 0)
    {
      write_to_pointers(data, new_data_size, block_data + current_ptr_num, BLOCK_SIZE / sizeof(uint32) - current_ptr_num);
    }

    ide_write(inode->single_indirect_block_pointer * BLOCK_SIZE, BLOCK_SIZE, block_data);
  }

  ide_write(gd->inodeTable + inode_number * sizeof(inode_t), sizeof(inode_t), inode);
  free(inode);
  return 0;
}

sint32 get_inode_number_by_path(const char *path)
{
  if (path == NULL)
  {
    return active_dir_inode_number;
  }

  // Start from the root directory
  uint32 current_inode_number = active_dir_inode_number;
  if (path[0] == '/')
  {
    current_inode_number = EXT2_ROOT_INODE;
  }

  // Tokenize the path
  char *token = strtok((char *)path, "/");
  while (token != NULL)
  {
    // Read the current directory's inode
    inode_t *current_inode = get_inode(current_inode_number);

    // Search for the directory entry with the current token
    uint32 target_inode_number = search_directory_entry(current_inode, token);
    if (target_inode_number == INODE_NOT_FOUND)
    {
      // Directory entry not found, handle accordingly
      return INODE_NOT_FOUND;
    }
    // Move to the next level
    current_inode_number = target_inode_number;
    // Get the next token
    token = strtok(NULL, "/");
    free(current_inode);
  }

  return current_inode_number;
}

// Function to search for a directory entry by name within a directory's inode
uint32 search_directory_entry(const inode_t *directory_inode, const char *name)
{
  char *data = getInodeData(*directory_inode);
  char *currentData = data;
  uint32 size = 0;
  while (size < directory_inode->size)
  {
    dirent_t *entry = currentData;
    if (strcmp(entry->name, name) == 0)
    {
      // Found the directory entry, return the inode number
      free(data);
      return entry->inode;
    }
    size += entry->dirent_size;
    currentData += entry->dirent_size;
  }
  // Directory entry not found, handle accordingly
  free(data);
  return INODE_NOT_FOUND;
}

sint32 create_directory(const char *parent_path, const char *folder_name)
{
  sint32 parent_inode_number = 0;
  if (strcmp(folder_name, "/"))
  {
    parent_inode_number = get_inode_number_by_path(parent_path);
    if (parent_inode_number == INODE_NOT_FOUND || parent_inode_number == NOT_FULL_PATH)
    { // later we can can add not full path
      return DIRECTORY_NOT_CREATED;
    }
  }

  uint32 folder_number = create_folder_inode();
  dirent_t current_entry;
  current_entry.inode = folder_number;
  current_entry.dirent_size = sizeof(dirent_t) + 1;
  current_entry.name_len = 1;
  current_entry.type = EXT2_FT_DIR;
  current_entry.name[0] = '.';
  current_entry.name[1] = 0;
  append_inode_data(folder_number, current_entry.dirent_size, &current_entry);

  if (strcmp(folder_name, "/"))
  {
    dirent_t directory_entry;
    directory_entry.inode = folder_number; // The inode number of the new directory
    directory_entry.dirent_size = sizeof(dirent_t) + strlen(folder_name);
    directory_entry.name_len = strlen(folder_name);
    directory_entry.type = EXT2_FT_DIR;
    strcpy(directory_entry.name, folder_name);
    directory_entry.name[directory_entry.name_len] = 0;
    append_inode_data(parent_inode_number, directory_entry.dirent_size, &directory_entry);

    dirent_t parent_entry;
    parent_entry.inode = parent_inode_number;
    parent_entry.dirent_size = sizeof(dirent_t) + 2; // 2 for ".." and the null terminator
    parent_entry.name_len = 2;
    parent_entry.type = EXT2_FT_DIR;
    parent_entry.name[0] = '.';
    parent_entry.name[1] = '.';
    parent_entry.name[2] = 0;
    append_inode_data(folder_number, parent_entry.dirent_size, &parent_entry);
  }

  return folder_number;
}

void printDirectories(uint32 inode_number)
{
  inode_t *inode = get_inode(inode_number);
  char *data = getInodeData(*inode);
  char *currentData = data;
  uint32 size = 0;
  while (size < inode->size)
  {
    dirent_t *entry = currentData;
    serial_write(SERIAL_COM1_BASE, entry->name, strlen(entry->name));
    printf("name: %s, inode: %d\n", entry->name, entry->inode);
    size += entry->dirent_size;
    currentData += entry->dirent_size;
  }
  free(inode);
  free(data);
}

sint32 create_file(const char *parent_path, const char *file_name)
{
  sint32 parent_inode_number = 0;
  parent_inode_number = get_inode_number_by_path(parent_path);
  if (parent_inode_number == INODE_NOT_FOUND || parent_inode_number == NOT_FULL_PATH)
  {
    return DIRECTORY_NOT_CREATED;
  }

  uint32 file_number = create_file_inode();
  dirent_t directory_entry;
  directory_entry.inode = file_number;
  directory_entry.dirent_size = sizeof(dirent_t) + strlen(file_name);
  directory_entry.name_len = strlen(file_name);
  directory_entry.type = EXT2_FT_FILE;
  strcpy(directory_entry.name, file_name);
  directory_entry.name[directory_entry.name_len] = 0;
  append_inode_data(parent_inode_number, directory_entry.dirent_size, &directory_entry);

  return file_number;
}

void free_inode(uint32 inode_number)
{
  uint8 inode_bit = get_bit(inode_number, inode_bitmap);
  if (!inode_bit)
    return;

  inode_t *inode = get_inode(inode_number);

  for (uint32 i = 0; i < AMOUNT_OF_DATA_BLOCKS; i++)
  {
    if (inode->direct_block_pointers[i])
    {
      deallocate_block(inode->direct_block_pointers[i]);
    }
    else
    {
      break;
    }
  }

  if (inode->single_indirect_block_pointer)
  {
    uint32 *block_data = malloc(BLOCK_SIZE);
    ide_read(inode->single_indirect_block_pointer * BLOCK_SIZE, BLOCK_SIZE, block_data);

    for (uint32 i = 0; i < BLOCK_SIZE / sizeof(uint32); i++)
    {
      if (block_data[i] != 0)
      {
        deallocate_block(block_data[i]);
      }
      else
      {
        break;
      }
    }

    free(block_data);
    deallocate_block(inode->single_indirect_block_pointer);
  }

  mark_bit(inode_number, inode_bitmap, 0);
  ide_write(gd->inodeBitmap, INODES_COUNT / BYTE_SIZE, inode_bitmap);
  sb->freeInodesCount++;
  gd->freeInodesCount++;
  ide_write(0, sizeof(superblock_t), sb);
  ide_write(BLOCK_SIZE, sizeof(group_descriptor_t), gd);

  free(inode);
}

uint8 delete_from_folder(const char *parent_path, const char *name)
{
  uint32 parent_inode_number = get_inode_number_by_path(parent_path);
  if (parent_inode_number == INODE_NOT_FOUND || parent_inode_number == NOT_FULL_PATH)
  {
    return DIRECTORY_NOT_CREATED;
  }

  inode_t *parent_inode = get_inode(parent_inode_number);
  char *parent_data = getInodeData(*parent_inode);
  char *current_data = parent_data;
  dirent_t current_entry = *((dirent_t *)parent_data);

  uint32 size = 0;
  while (size < parent_inode->size)
  {
    if (!strcmp(current_entry.name, name))
    {
      // delete the directory entry
      uint32 entry_size = current_entry.dirent_size;
      memmove(current_data, current_data + entry_size, parent_inode->size - size - entry_size);
      parent_inode->size -= entry_size;
      uint32 inode_number = current_entry.inode;

      if (current_entry.type == EXT2_FT_DIR)
      {
        delete_directory_recursive(inode_number);
      }

      // change the modification time here
      write_inode_data(parent_inode_number, parent_inode->size, parent_data);

      free(parent_inode);
      free(parent_data);

      return OPERATION_SUCCESSFULL;
    }

    size += current_entry.dirent_size;
    current_data += current_entry.dirent_size;
    current_entry = *((dirent_t *)current_data);
  }

  free(parent_inode);
  free(parent_data);

  return INODE_NOT_FOUND;
}

void delete_directory_recursive(uint32 parent_inode_number)
{
  inode_t *parent_inode = get_inode(parent_inode_number);
  char *data = getInodeData(*parent_inode);
  char *current_data = data;
  uint32 size = 0;

  while (size < parent_inode->size)
  {
    dirent_t *entry = current_data;

    if (strcmp(entry->name, ".") && strcmp(entry->name, ".."))
    {
      uint32 entry_inode_number = entry->inode;

      if (entry->type == EXT2_FT_DIR)
      {
        delete_directory_recursive(entry_inode_number);
      }

      free_inode(entry_inode_number);
    }

    size += entry->dirent_size;
    current_data += entry->dirent_size;
  }

  free(parent_inode);
  free(data);
}

sint32 change_directory(const char *dir_path)
{
  if (strcmp(dir_path, "/"))
  {
    sint32 result = get_inode_number_by_path(dir_path);
    if (result < 0)
    {
      return result;
    }

    inode_t *inode = get_inode(result);
    if (inode->mode == FILE_MODE)
    {
      return INCORRECT_TYPE;
    }
    free(inode);

    active_dir_inode_number = result;
  }
  else
  {
    active_dir_inode_number = EXT2_ROOT_INODE;
  }
}

sint32 write_to_file(const char *file_path, uint32 data, uint32 size)
{
  sint32 inode_number = get_inode_number_by_path(file_path);
  if (inode_number < 0)
  {
    return inode_number;
  }

  inode_t *inode = get_inode(inode_number);
  if (inode->mode == FOLDER_MODE)
  {
    return INCORRECT_TYPE;
  }
  free(inode);

  write_inode_data(inode_number, size, data);

  return OPERATION_SUCCESSFULL;
}

sint32 get_file_content(const char *file_path)
{
  sint32 inode_number = get_inode_number_by_path(file_path);
  if (inode_number < 0)
  {
    return inode_number;
  }

  inode_t *inode = get_inode(inode_number);
  if (inode->mode == FOLDER_MODE)
  {
    return INCORRECT_TYPE;
  }

  uint32 inode_data = getInodeData(*inode);
  free(inode);
  return inode_data;
}

void print_current_directory()
{
  inode_t *inode = get_inode(active_dir_inode_number);
  char *data = getInodeData(*inode);
  char *currentData = data;
  uint8 first = 1;
  uint32 size = 0;
  while (size < inode->size)
  {
    dirent_t *entry = currentData;
    if (strcmp(entry->name, ".") && strcmp(entry->name, ".."))
    {
      if (first)
      {
        printf(entry->name);
        first = 0;
      }
      else
      {
        printf(", %s", entry->name);
      }
    }
    size += entry->dirent_size;
    currentData += entry->dirent_size;
  }
  if (first) // there were no files in the directory
  {
    printf("No files in the directory");
  }
  free(inode);
  free(data);
  printf("\n");
}

sint32 copy_file(const char *source_file_path, const char *destination_parent_path, const char *destination_file_name)
{
  sint32 source_inode_number = get_inode_number_by_path(source_file_path);
  if (source_inode_number < 0)
  {
    return source_inode_number;
  }

  inode_t *source_inode = get_inode(source_inode_number);
  if (source_inode->mode == FOLDER_MODE)
  {
    return INCORRECT_TYPE;
  }

  sint32 parent_inode_number = 0;
  parent_inode_number = get_inode_number_by_path(destination_parent_path);
  if (parent_inode_number == INODE_NOT_FOUND || parent_inode_number == NOT_FULL_PATH)
  {
    return DIRECTORY_NOT_CREATED;
  }

  uint32 destination_inode_number = create_inode(source_inode);
  sint32 original_data = get_file_content(source_file_path); // im using this instead of getInodeData because for some reason when i get the object from the inode pointer, the first direct block pointer is reset to 0
  write_inode_data(destination_inode_number, source_inode->size, original_data);
  free(source_inode);
  free(original_data);

  dirent_t directory_entry;
  directory_entry.inode = destination_inode_number;
  directory_entry.dirent_size = sizeof(dirent_t) + strlen(destination_file_name);
  directory_entry.name_len = strlen(destination_file_name);
  directory_entry.type = EXT2_FT_FILE;
  strcpy(directory_entry.name, destination_file_name);
  directory_entry.name[directory_entry.name_len] = 0;
  append_inode_data(parent_inode_number, directory_entry.dirent_size, &directory_entry);

  return destination_inode_number;
}