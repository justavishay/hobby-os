#ifndef INCLUDE_STRING_H
#define INCLUDE_STRING_H

char *int_to_string(int num, char *str, int base);
void reverse_string(char str[], int length);
uint32 strsplit(char *str, char delim);
void *memset(void *dst, char c, uint32 n);
void *memcpy(void *dst, const void *src, uint32 n);
void *memmove(void *dest, const void *src, uint32 n);
uint8 strncmp(char *a, char *b, uint32 n);
uint8 memcmp(uint8 *s1, uint8 *s2, uint32 n);
uint32 strlen(const char *s);
uint8 strcmp(const char *s1, char *s2);
uint8 strcpy(char *dst, const char *src);
void strcat(char *dest, const char *src);
void strncat(char *dest, const char *src, int n);
uint8 isspace(char c);
uint8 isalpha(char c);
char upper(char c);
char lower(char c);
void itoa(char *buf, int base, int d);
char *strchr(const char *str, int character);
char *strrchr(const char* str, int character);
char *strtok(char *str, const char *delim);

#endif